package exammmm.ex1;

class U {
public void p(){}
}

class I extends U{
    K k;

    private long t;
    public void f(){}

}

class J{
    public void i(I i){}
}

class N {
    I i;

}

class K{
    L l;
    M m;

    K(M m){
        this.m=m;
        this.l= new L();
    }
}

class L{
    public void metA(){}
}

class M{
    public void metB(){}
}

