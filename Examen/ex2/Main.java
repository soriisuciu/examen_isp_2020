package exammmm.ex2;

public class Main {
    public static void main(String[] args) {
        MultipleThread MThread1 = new MultipleThread("MThread1");
        MultipleThread MThread2 = new MultipleThread("MThread2");

        Thread thread1 = new Thread(MThread1);
        Thread thread2 = new Thread(MThread2);

        thread1.start();
        thread2.start();
    }
}

class MultipleThread implements Runnable {
    private String name;
    private int number;

    public MultipleThread(String name) {
        this.name = name;
        this.number = 1;
    }


    @Override
    public void run() {

        while (this.number <= 3) { //each thread should write 3 messages

            System.out.println("["+this.name+"]" + " - [" + System.currentTimeMillis() + "]");
            try {
                Thread.sleep(7000);
                this.number++;
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}